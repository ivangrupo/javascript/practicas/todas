/* JS DOCUMENT */

window.addEventListener("load", () => {

    // var a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    var a=[];

    for (var c=0; c<10; c++){
		n=parseInt(prompt("Introduce 10 números"));
		a[c]=n;
		// a.push(n);	Es lo mismo que a[c]=n;
	}

    console.log(a);

    var pares = 0;

    for (var c = 0; c < a.length; c++) {

        if ((a[c] % 2) == 0) {
            pares++;

        }
    }

    console.log(pares);

    var maximo = a[0];

    for (var c = 1; c < a.length; c++) {

        if (maximo < a[c]) {
            maximo = a[c];
        }
    }

    console.log(maximo);

    var minimo = a[0];

    for (var c = 1; c < a.length; c++) {

        if (minimo > a[c]) {
            minimo = a[c];
        }
    }

    console.log(minimo);

    document.querySelector("header").innerHTML+= "Números pares: " + pares;
    document.querySelector("main").innerHTML+= "El máximo es: " + maximo;
    document.querySelector("footer").innerHTML+= "El mínimo es: " + minimo;

});