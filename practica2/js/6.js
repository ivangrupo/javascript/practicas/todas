/* JS DOCUMENT */

// DECLARAR E INICIAR VARIABLES

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

// LEER DNI Y LETRA

var dni = prompt("Escribe tu numero de DNI (Sólo el número)");
var letra = prompt("Escribe la letra de tu DNI en mayusculas").toUpperCase();

// OPERACIÓN

if (dni < 0 || dni > 99999999) {
    alert("El número proporcionado no es válido");
} else {
    var buscador = letras[dni % 23];
    if (buscador != letra) {
        alert("La letra introducida no es correcta");
    } else {
        alert("El número y letra de DNI son correctos");
    }
}