﻿window.addEventListener("load", () => {

    var botones=document.querySelectorAll("button");
    var resultado=document.querySelector("#resultado");

    for(var c=0;c<botones.length;c++){
    	botones[c].addEventListener("click",(event) => {
    	  var id=event.target.id;
    	  switch (id){
    	  	case 'sumar':
    	  		resultado.value=sumar(document.querySelector("#suma1").value, document.querySelector("#suma2").value);
    	  		break;
    	  	case 'restar':
    	  		resultado.value=restar(document.querySelector("#resta1").value, document.querySelector("#resta2").value);
    	  		break;
    	  	case 'negar':
    	  		resultado.value=negar(document.querySelector("#negar1").value);
    	  		break;
    	  	case 'producto':
    	  		resultado.value=producto(document.querySelector("#producto1").value, document.querySelector("#producto2").value);
    	  		break;
    	  		case 'cociente':
    	  		resultado.value=cociente(document.querySelector("#cociente1").value, document.querySelector("#cociente2").value);
    	  		break;
    	  	case 'resto':
    	  		resultado.value=resto(document.querySelector("#resto1").value, document.querySelector("#resto2").value);
    	  		break;
    	  	}
    	});
    }
});


function sumar(n1,n2){
	var r=0;
	r= parseInt(n1) + parseInt(n2);
	return r;
}

function restar(n1,n2){
	var r=0;
	r= n1 - n2;
	return r;
}

function negar(n1){
	var r=0;
	r= document.querySelector("#resultado").innerHTML+= "-" + n1;
	return r;
}

function producto(n1,n2){
	var r=0;
	r= parseInt(n1) * parseInt(n2);
	return r;
}

function cociente(n1,n2){
	var r=0;
	r= parseInt(n1) / parseInt(n2);
	return r;
}

function resto(n1,n2){
	var r=0;
	r= parseInt(n1) % parseInt(n2);
	return r;
}
