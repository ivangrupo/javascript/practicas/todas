/* JS DOCUMENT */

// Sustituimos el onload del HTML por una función anónima para cargar el Javascript después de que cargue toda la web
window.addEventListener("load", () => {

	alert("Hola Mundo!");	// Mensaje de alerta

	alert("Soy el primer script");	// Mensaje de alerta

	document.querySelector("header").innerHTML="<p>Es necesario tener activado el soporte JavaScript en tu navegador</p>";

	// Accedemos al header utilizando querySelector y con innerHTML insertamos el texto
});